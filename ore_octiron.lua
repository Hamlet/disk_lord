--[[
    Disk Lord - Adds Discworld's like things.
    Copyright (C) 2018  Hamlet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
--]]


minetest.register_node("disk_lord:stone_with_octiron", {
	description = "Octiron Ore",
	tiles = {
		"default_stone.png^default_mineral_diamond.png^[colorize:magenta:95"
	},
	sounds = default.node_sound_stone_defaults(),
	groups = {cracky = 1, level = 2},
	on_construct = function(pos)
		local swap_node = false
		local north = {x = pos.x, y = pos.y, z = (pos.z + 1)}
		local north_east = {x = (pos.x + 1), y = pos.y, z = (pos.z + 1)}
		local east = {x = (pos.x + 1), y = pos.y, z = pos.z}
		local south_east = {x = (pos.x + 1), y = pos.y, z = (pos.z - 1)}
		local south = {x = pos.x, y = pos.y, z = (pos.z - 1)}
		local south_west = {x = (pos.x - 1), y = pos.y, z = (pos.z - 1)}
		local west = {x = (pos.x - 1), y = pos.y, z = pos.z}
		local north_west = {x = (pos.x - 1), y = pos.y, z = (pos.z + 1)}

		north = minetest.get_node(north).name
		north_east = minetest.get_node(north_east).name
		east = minetest.get_node(east).name
		south_east = minetest.get_node(south_east).name
		south = minetest.get_node(south).name
		south_west = minetest.get_node(south_west).name
		west = minetest.get_node(west).name
		north_west = minetest.get_node(north_west).name

		local neighbours = {
			north, north_east, east, south_east, south, south_west, west,
			north_west
		}

		print(dump(neighbours))

		for n = 1, 8 do
			if (neighbours[n] == "default:lava_source")
			or (neighbours[n] == "default:lava_flowing") then
				swap_node = true
			end
		end

		if (swap_node == true) then
			minetest.get_node_timer(pos):start(10)
		end
	end,

	on_timer = function(pos, elapsed)
		local quantity = math.random(1, 3)
		minetest.set_node(pos, {name = "air"})
		minetest.add_item(pos, "disk_lord:octiron_ingot" .. " " .. quantity)
	end,

	on_blast = function() end,
})

minetest.register_craftitem("disk_lord:octiron_ingot", {
	description = "Octiron Ingot",
	inventory_image = "default_steel_ingot.png^[colorize:black:200",
})

minetest.register_node("disk_lord:octiron_block", {
	description = "Octiron Block",
	tiles = {"default_steel_block.png^[colorize:black:200"},
	is_ground_content = false,
	groups = {cracky = 1, level = 2},
	sounds = nil,
	on_blast = function() end,
})

minetest.register_craft({
	output = "disk_lord:octiron_block",
	recipe = {
		{"disk_lord:octiron_ingot",
		"disk_lord:octiron_ingot",
		"disk_lord:octiron_ingot"},
		{"disk_lord:octiron_ingot",
		"disk_lord:octiron_ingot",
		"disk_lord:octiron_ingot"},
		{"disk_lord:octiron_ingot",
		"disk_lord:octiron_ingot",
		"disk_lord:octiron_ingot"},
	}
})

minetest.register_alias("OctironOre", "disk_lord:stone_with_octiron")
minetest.register_alias("OctironIngot", "disk_lord:octiron_ingot")
minetest.register_alias("OctironBlock", "disk_lord:octiron_block")


local octiron = {
	ore_type = "blob",
	ore = "disk_lord:stone_with_octiron",
	wherein = "default:stone",
	clust_scarcity = (math.random(15, 16)) *
							(math.random(15, 16)) *
							(math.random(15, 16)),
	clust_num_ores = math.random(3, 12),
	clust_size = 3,
	y_min = -31000,
	y_max = -300,
	noise_threshold = math.random(0.0, 1.0),
	noise_params = {
		offset = 0,
		scale = 0,
		spread = {
			x = math.random(1, 250),
			y = math.random(1, 250),
			z = math.random(1, 250)
		},
		seed = math.random(0, 9223372036854775807),
		octaves = 6,
		persist = 0.6
	},
}

minetest.register_ore(octiron)
