--[[
    Disk Lord - Adds Discworld's like things.
    Copyright (C) 2018  Hamlet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
--]]

local dying = ""

local function random_message(dying)
	local prefix = "The Reaper: "

	local messages = {
		"HELLO " .. dying .. " WOULD YOU LIKE TO REST FOR A WHILE?",
		"I WAS HAVING A DRINK AT A PARTY AND I THOUGHT TO INVITE YOU.",
		"SURPRISE! IT IS YOUR DEATHDAY! HAPPY DEATHDAY TO YOU!",
		"SO LET'S MAKE THIS QUICK: I HAVE A LOT OF WORK TO DO.",
		"YOU WERE NOT ON MY LIST TODAY, BUT I'LL MAKE AN EXCEPTION.",
		"OH DEAR, I LOVE WHEN PEOPLE ARE NOT LATE ON DATES.",
		"HAVE YOU CALLED ME?",
		"LET'S MAKE CLEAR ONE THING: NO CHESS PLAYING WITH ME. I HATE CHESS.",
		"DO YOU KNOW THAT SONG... \"DON'T FEAR THE REAPER\"?",
		"OH, YES... " .. "\"" .. dying .. "\" I WAS EXPECTING YOU.",
		"IT SEEMS THAT THE SAND IN YOUR HOURGLASS IS ALMOST FINISHED...",
		"PLEASE BE NICE AND DON'T WASTE MY TIME, NOW IF YOU WOULD LAY FOREVER...",
		"GETTING TIRED OF BREATHING, ISN'T IT?",
		"I SWEAR THAT I NEED SOME HOLIDAYS.",
		"IT DOESN'T MATTER IF YOU HAVEN'T \"SEEN THE LIGHT\", IT'S TIME TO DIE.",
		"DO DIES DIE? *snickers* ALRIGHT, THAT WAS AWFUL."
	}

	local message_number = math.random(1, #messages)

	local message = minetest.colorize("grey", messages[message_number])

	message = prefix .. message


	return message
end


local function owner_is_dead(dying)
	local prefix = "The Reaper: "

	local messages = {
		"ANOTHER ONE BITES THE DUST.",
		"\"SIC TRANSIT\"... I CAN'T REMEMBER THE REST. BAH, IT DOESN'T MATTER.",
		"NO ARGUING, NO WHINING. IF ONLY IT COULD ALWAYS BE LIKE THIS...",
		"ANOTHER SATISFIED CUSTOMER. NOW, WHO'S NEXT?",
		"YOU WOULD NOT BELIEVE WHAT PEOPLE THINK WHEN THEY DIE.",
		"I'VE JUST GOT THE TIME FOR A MOJITO.",
		"STICKS AND STONES WILL NOT BREAK THESE BONES. *laughter*",
		"MAYBE I SHOULD WEAR ANOTHER SHADE OF GREY."
	}

	local message_number = math.random(1, #messages)

	local message = minetest.colorize("grey", messages[message_number])

	message = prefix .. message

	return message
end


local function owner_is_alive(dying)
	local prefix = "The Reaper: "

	local messages = {
		"TSK. SEE YOU SOON.",
		"IT DRIVES ME NUTS WHEN THEY BEHAVE LIKE THIS. *sigh*",
		"THERE IS NO RESPECT FOR WORKERS ANYMORE",
		"DON'T THINK THAT YOU'RE THAT SMART: TIME IS ON MY SIDE.",
		"PFFT, ANOTHER ONE THAT WILL GO AROUND BRAGGING ABOUT THIS.",
		"*sigh* I HATE WASTING MY TIME.",
		"IT'S ALWAYS LIKE THIS WITH BLIND DATES, I WONDER WHAT'S WRONG WITH ME.",
		"THAT'S WHAT I GET FOR TRUSTING PEOPLE."
	}

	local message_number = math.random(1, #messages)

	local message = minetest.colorize("grey", messages[message_number])

	message = prefix .. message

	return message
end



mobs:register_mob("disk_lord:reaper", {
	type = "npc",
	owner_loyal = false,
	passive = true,
	hp_min = 9999,
	hp_max = 9999,
	collisionbox = {-0.4, -1, -0.4, 0.4, 0.75, 0.4},
	visual = "mesh",
	mesh = "disk_lord_character.b3d",
	textures = {
		"disk_lord_reaper.png",
		"disk_lord_transparent.png",
		"disk_lord_scythe.png",
		"disk_lord_transparent.png"
	},
	visual_size = {x=1, y=1},
	makes_footstep_sound = false,
	view_range = 14,
	walk_velocity = 5.2,
	walk_chance = 100,
	run_velocity = 5.2,
	damage = 9999,
	armor = 0,
	fly = true,
	drawtype = "front",
	water_damage = 0,
	lava_damage = 0,
	light_damage = 0,
	attacks_monsters = false,
	attack_type = "dogfight",
	animation = {
		stand_start = 0,
		stand_end = 79,
		stand_speed = 30,
		punch_start = 189,
		punch_end = 198,
		punch_speed = 30
	},

	on_spawn = function(self)
		self.owner = dying
		self.owner_ObjectRef = minetest.get_player_by_name(self.owner)
		dying = ""
		self.order = "follow"

		self.object:set_properties({
			owner = self.owner,
			owner_ObjectRef = self.owner_ObjectRef,
			order = self.order,
			collide_with_objects = false
		})

		minetest.chat_send_player(self.owner, random_message(self.owner))
		
	return true
	end,

	do_custom = function(self, dtime)

		local despawn = "true"
		local hp_recovered = "true"

		if (self.owner_ObjectRef ~= nil) then
			despawn = self.owner_ObjectRef:get_attribute("disk_lord_reaper")
			hp_recovered = self.owner_ObjectRef:get_attribute("hp_recovered")
		end

		if (despawn ~= "false") then
			if (hp_recovered ~= "true") then
				minetest.chat_send_player(self.owner, owner_is_dead(self.owner))
			else
				minetest.chat_send_player(self.owner, owner_is_alive(self.owner))
			end

			self.object:remove()
		end
	end
})

mobs:register_egg("disk_lord:reaper", "Reaper",	"bones_front.png")
mobs:alias_mob("mobs:reaper", "disk_lord:reaper")


minetest.register_on_player_hpchange(function(player, hp_change)
	if (player ~= nil) then
		local current_hp = player:get_hp()

		current_hp = current_hp + hp_change

		if (current_hp < 0) then
			current_hp = 0

		elseif (current_hp > 20) then
			current_hp = 20

		end

		local spawn_reaper = player:get_attribute("disk_lord_reaper")

		if (spawn_reaper ~= "false") then
			if (current_hp <= 5) and (current_hp > 0) then
				dying = player:get_player_name()
				player:set_attribute("disk_lord_reaper", "false")
				player:set_attribute("hp_recovered", "false")

				local position = player:get_pos()
				position.y = position.y + 1
				minetest.add_entity(position, "disk_lord:reaper")
			end

		else
			if (current_hp > 9) then
				dying = ""
				player:set_attribute("disk_lord_reaper", "true")
				player:set_attribute("hp_recovered", "true")
			end
		end
	end
end)

minetest.register_on_dieplayer(function(player)
	local spawn_reaper = player:get_attribute("disk_lord_reaper")

	if (spawn_reaper == "false") then
		player:set_attribute("disk_lord_reaper", "true")
	end
end)
