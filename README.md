### DISK LORD
![Disk Lord's screenshot](screenshot.png)<br>
**_Adds Discworld's like things._**

**Version:** 0.2.1<br>
**Source code's license:** GPL v3.0<br>
**Multimedia files' licenses:** CC BY-SA v4.0 International - CC0 v1.0


**Dependencies:** default (found in Minetest Game)<br>
**Supported:** mobs (Mobs Redo)


## Crafting Recipes

* Octiron Ingots
	* Place an octiron ore node next to a lava node.

* Octiron Block
	* Place nine octiron ingots in the crafting grid.

O: Octiron Ingot<br>

OOO<br>
OOO<br>
OOO


* Sapient Apple Tree's Sapling
	* Place eight octiron blocks in the crafting grid, place a sapling in the central slot.

B: Octiron Block<br>
S: Sapling

BBB<br>
BSB<br>
BBB


* The Luggage (only available if Mobs Redo is detected)
	* Place eight Sapient Apple Tree's wood planks in the crafting grid.
	* If you are playing in creative mode, use the spawning "egg".
	* If you have the "give" privilege, use /spawnentity mobs:luggage

W: Sapient Apple Tree's wood plank<br>

WWW<br>
W_W<br>
WWW


* The Reaper (only available if Mobs Redo is detected)
	* Will spawn if HP <= 5 and despawn if HP > 9.
	* If you are playing in creative mode, use the spawning "egg".
	* If you have the "give" privilege, use /spawnentity mobs:reaper


### Installation

Unzip the archive, rename the folder to disk_lord and place it in<br>
../minetest/mods/

If you only want this to be used in a single world, place it in<br>
../minetest/worlds/WORLD_NAME/worldmods/

GNU+Linux - If you use a system-wide installation place it in<br>
~/.minetest/mods/

For further information or help see:<br>
https://wiki.minetest.net/Help:Installing_Mods
