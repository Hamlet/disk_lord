--[[
    Disk Lord - Adds Discworld's like things.
    Copyright (C) 2018  Hamlet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
--]]


local attack_state = function(self, target)
   self.visual = "mesh"
	self.rotate = 135
   self.visual_size = {x = 10, y = 10}
   self.base_texture[6] = "disk_lord_inside.png"
   self.object:set_properties({
      visual = self.visual,
      rotate = self.rotate,
      visual_size = self.visual_size,
      textures = self.base_texture,
   })
   self.attack = target
   self.state = "attack"
end


local peaceful_state = function(self)
   self.visual = "cube"
   self.visual_size = {x = 1, y = 1}
	self.rotate = 0
   self.base_texture[6] = "default_chest_side.png"
	self.sleep_counter = 300000
   self.object:set_properties({
		sleep_counter = self.sleep_counter,
		rotate = self.rotate,
      visual = self.visual,
      visual_size = self.visual_size,
      textures = self.base_texture,
   })
end


local function heal_over_time(self, dtime)
	if (self.state ~= "attack") and (self.state ~= "runaway") then
		-- For backward compatibility
		if (self.heal_counter == nil) or (self.initial_hp == nil) then
			self.heal_counter = 2.0
			-- used for health recovery

			self.initial_hp = math.random(self.hp_min, self.hp_max)
			-- used as reference when recovering health

			self.object:set_properties({
				heal_counter = self.heal_counter,
				initial_hp = self.initial_hp
			})
		end

		-- recover 1HP every 2 seconds
		if (self.health < self.initial_hp)
		and (self.state ~= "attack")
		and (self.state ~= "runaway")
		then
			if (self.heal_counter > 0) then
				self.heal_counter = self.heal_counter - dtime

				self.object:set_properties({
					heal_counter = self.heal_counter
				})

			else
				self.heal_counter = 2.0
				self.health = self.health + 1
				self.object:set_hp(self.health)

				self.object:set_properties({
					heal_counter = self.heal_counter
				})
				print(self.health)
			end
		end
	end
end


mobs:register_mob("disk_lord:luggage", {
   --nametag = "Luggage",
   type = "npc",
   hp_min = 40,
   hp_max = 60,
   armor = 100,
   walk_velocity = 3,
   run_velocity = 5.2,
   walk_chance = 75,
	sleep_counter = 300000,
	chage_shape = true,
   jump = false,
   view_range = 6,
   damage = 4,
   fear_height = 3,
   fall_speed = -99,
   fall_damage = true,
   lava_damage = 10,
   floats = 1,
   follow = {
      "default:gold_lump", "default:gold_ingot", "default:mese_crystal",
      "default:mese", "default:diamond", "default:diamondblock"
   },
   reach = 2,
   owner_loyal = true,
   attack_type = "dogfight",
   blood_amount = 0,
   pathfinding = 1,
   immune_to = {
      {"default:gold_lump", 10},
      {"default:gold_ingot", 10},
      {"default:mese_crystal", 15},
      {"default:mese", 90},
      {"default:diamond", 20},
      {"default:diamondblock", 180}
   },
   makes_footstep_sound = true,
   sounds = {
      war_cry = "default_chest_open",
      attack = "default_chest_open",
      death = "default_chest_close"
   },
	sound_handle = nil,
   visual = "cube",
   visual_size = {x = 1, y = 1},
   collisionbox = {-0.4, -0.5, -0.4, 0.4, 0.5, 0.4},
   selectionbox = {-0.4, -0.5, -0.4, 0.4, 0.5, 0.4},
   textures = {
		"default_chest_top.png",
		"default_chest_top.png",
		"default_chest_side.png",
		"default_chest_side.png",
		"default_chest_front.png",
      "default_chest_side.png"
   },
   mesh = "disk_lord_open.obj",

   on_rightclick = function(self, clicker)

		if (self.tamed == false)
		or (clicker:get_player_name() ~= self.owner)
		then
			local wield_item = clicker:get_wielded_item()
			wield_item = wield_item:get_name()
			local follows = self.follow
			local angry = true

			for n = 1, 6 do
				if (follows[n] == wield_item) then
					angry = false
				end
			end

			if (angry == true) then
				attack_state(self, clicker)
			end

		end

		if mobs:feed_tame(self, clicker, 10, false, true) then
			return
		end

		if mobs:protect(self, clicker) then
			return
		end

		if (self.tamed == true)
		and (self.owner == clicker:get_player_name())
		then
			mobs:capture_mob(self, clicker, 100, 100, 100, false, nil)
		end
   end,

   do_punch = function(self, hitter)
		if (hitter ~= nil) then
			if (self.tamed == true) then
				if (hitter:get_player_name() ~= self.owner) then
				   if (self.health > 0) and (self.state ~= "attack") then
						attack_state(self, hitter)
					end
				end
			else
			   if (self.health > 0) and (self.state ~= "attack") then
					attack_state(self, hitter)
				end
		   end
		end
   end,

   do_custom = function(self, dtime)
		heal_over_time(self, dtime)

		if (self.state == "attack") then
			self.change_shape = true
			self.visual = "mesh"
			self.rotate = 135
			self.visual_size = {x = 10, y = 10}
			self.base_texture[6] = "disk_lord_inside.png"
			self.object:set_properties({
				change_shape = self.change_shape,
				visual = self.visual,
				rotate = self.rotate,
				visual_size = self.visual_size,
				textures = self.base_texture,
			})

		elseif (self.state ~= "attack") and (self.change_shape == true) then
			self.change_shape = false
			self.visual = "cube"
			self.visual_size = {x = 1, y = 1}
			self.rotate = 0
			self.base_texture[6] = "default_chest_side.png"
			self.object:set_properties({
				change_shape = self.change_shape,
				rotate = self.rotate,
				visual = self.visual,
				visual_size = self.visual_size,
				textures = self.base_texture,
			})
		end

		if (self.state == "stand") and (self.sound_handle == nil) then
			if (self.sleep_counter > 0) then
				self.sleep_counter = (self.sleep_counter - 1)
				self.object:set_properties({
					sleep_counter = self.sleep_counter
				})
			else
				self.sleep_counter = 300000
				self.nametag = "Zzz..."
				self.sound_handle = minetest.sound_play("disk_lord_snoring", {
					object = self.object,
					gain = 1.0,
					max_hear_distance = 10,
					loop = true
				})
				self.object:set_properties({
					nametag = self.nametag,
					sleep_counter = self.sleep_counter,
					sound_handle = self.sound_handle
				})
			end
		elseif (self.state ~= "stand") and (self.sound_handle ~= nil) then
			self.nametag = "Luggage"
			minetest.sound_stop(self.sound_handle)
			self.sound_handle = nil
			self.object:set_properties({
				nametag = self.nametag,
				sound_handle = self.sound_handle
			})
		end
   end,

	on_spawn = function(self, pos)
		peaceful_state(self)
		self.order = "follow"

		self.object:set_properties({
			order = self.order
		})
	end,
})

mobs:register_egg("disk_lord:luggage", "Luggage",
	"default_chest_front.png", false)

mobs:alias_mob("mobs:luggage", "disk_lord:luggage")
