--[[
    Disk Lord - Adds Discworld's like things.
    Copyright (C) 2018  Hamlet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
--]]


--
-- General variables
--

local minetest_log_level = minetest.settings:get("debug_log_level")
local mod_load_message = "[Mod] Disk Lord [v0.2.1] loaded."
local mod_path = minetest.get_modpath("disk_lord")


--
-- Files to be loaded
--

dofile(mod_path .. "/ore_octiron.lua")
dofile(mod_path .. "/sapient_tree.lua")

if minetest.get_modpath("mobs") then
	dofile(mod_path .. "/mobs_luggage.lua")
	dofile(mod_path .. "/mobs_reaper.lua")
end


--
-- Minetest engine debug logging
--

if (minetest_log_level == nil) or (minetest_log_level == "action") or
	(minetest_log_level == "info") or (minetest_log_level == "verbose") then

	minetest.log("action", mod_load_message)
end
