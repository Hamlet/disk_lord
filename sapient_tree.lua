--[[
    Disk Lord - Adds Discworld's like things.
    Copyright (C) 2018  Hamlet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
--]]


local generate_sapient_tree = function(pos)

	local node_trunk = "disk_lord:sapient_appletree"
	local node_leaves = "disk_lord:sapient_leaves"
	local node_fruit = "disk_lord:sapient_apple"

	-- Trunk's generation

	local trunk_height = math.random(4, 5)
	local tree_base = true

	for n = 1, trunk_height do
		if (tree_base == true) then
			tree_base = false
			minetest.set_node(pos, {name = node_trunk})
		elseif (tree_base == false) then
			pos.y = (pos.y + 1)
			local position = {x = pos.x, y = pos.y, z = pos.z}
			minetest.set_node(position, {name = node_trunk})

		end
	end

	-- Branches' generation

	local branches_number = math.random(2, 4)

	local north_east = {x = (pos.x + 1), y = pos.y, z = (pos.z + 1)}
	local south_east = {x = (pos.x + 1), y = pos.y, z = (pos.z - 1)}
	local south_west = {x = (pos.x - 1), y = pos.y, z = (pos.z - 1)}
	local north_west = {x = (pos.x - 1), y = pos.y, z = (pos.z + 1)}

	local north_east_toggle = false
	local south_east_toggle = false
	local south_west_toggle = false
	local north_west_toggle = false

	for n = 1, branches_number do

		::retry_branch::

		local branch_position = math.random(1, 4)

		if (branch_position == 1) and (north_east_toggle == false) then
			north_east_toggle = true
			minetest.set_node(north_east, {name = node_trunk})
			
			local leaves_1 = {
				x = (north_east.x + 1),
				y = north_east.y,
				z = north_east.z
			}

			local leaves_2 = {
				x = (north_east.x + 1),
				y = north_east.y,
				z = (north_east.z + 1)
			}

			local leaves_3 = {
				x = north_east.x,
				y = north_east.y,
				z = (north_east.z + 1)
			}

			minetest.set_node(leaves_1, {name = node_leaves})
			minetest.set_node(leaves_2, {name = node_leaves})
			minetest.set_node(leaves_3, {name = node_leaves})

		elseif (branch_position == 1) and (north_east_toggle == true) then
			goto retry_branch
		end

		if (branch_position == 2) and (south_east_toggle == false) then
			south_east_toggle = true
			minetest.set_node(south_east, {name = node_trunk})

			local leaves_1 = {
				x = (south_east.x + 1),
				y = south_east.y,
				z = south_east.z
			}

			local leaves_2 = {
				x = (south_east.x + 1),
				y = south_east.y,
				z = (south_east.z - 1)
			}

			local leaves_3 = {
				x = south_east.x,
				y = south_east.y,
				z = (south_east.z - 1)
			}

			minetest.set_node(leaves_1, {name = node_leaves})
			minetest.set_node(leaves_2, {name = node_leaves})
			minetest.set_node(leaves_3, {name = node_leaves})

		elseif (branch_position == 2) and (south_east_toggle == true) then
			goto retry_branch
		end

		if (branch_position == 3) and (south_west_toggle == false) then
			south_west_toggle = true
			minetest.set_node(south_west, {name = node_trunk})

			local leaves_1 = {
				x = (south_west.x - 1),
				y = south_west.y,
				z = south_west.z
			}

			local leaves_2 = {
				x = (south_west.x - 1),
				y = south_west.y,
				z = (south_west.z - 1)
			}

			local leaves_3 = {
				x = south_west.x,
				y = south_west.y,
				z = (south_west.z - 1)
			}

			minetest.set_node(leaves_1, {name = node_leaves})
			minetest.set_node(leaves_2, {name = node_leaves})
			minetest.set_node(leaves_3, {name = node_leaves})

		elseif (branch_position == 3) and (south_west_toggle == true) then
			goto retry_branch
		end

		if (branch_position == 4) and (north_west_toggle == false) then
			north_west_toggle = true
			minetest.set_node(north_west, {name = node_trunk})

			local leaves_1 = {
				x = (north_west.x - 1),
				y = north_west.y,
				z = north_west.z
			}

			local leaves_2 = {
				x = (north_west.x - 1),
				y = north_west.y,
				z = (north_west.z + 1)
			}

			local leaves_3 = {
				x = north_west.x,
				y = north_west.y,
				z = (north_west.z + 1)
			}

			minetest.set_node(leaves_1, {name = node_leaves})
			minetest.set_node(leaves_2, {name = node_leaves})
			minetest.set_node(leaves_3, {name = node_leaves})

		elseif (branch_position == 4) and (north_west_toggle == true) then
			goto retry_branch
		end
	end

	local apples_number = math.random(0, 4)

	if (apples_number > 0) then

		local north = {x = pos.x, y = pos.y, z = (pos.z + 1)}
		local east = {x = (pos.x + 1), y = pos.y, z = pos.z}
		local south = {x = pos.x, y = pos.y, z = (pos.z - 1)}
		local west = {x = (pos.x - 1), y = pos.y, z = pos.z}

		local north_toggle = false
		local east_toggle = false
		local south_toggle = false
		local west_toggle = false

		for n = 1, apples_number do
			::retry_apple::

			local apple_position = math.random(1, 4)

			if (apple_position == 1) and (north_toggle == false) then
				north_toggle = true
				minetest.set_node(north, {name = node_fruit})
			
			elseif (apple_position == 1) and (north_toggle == true) then
				goto retry_apple
			end

			if (apple_position == 2) and (east_toggle == false) then
				east_toggle = true
				minetest.set_node(east, {name = node_fruit})
			
			elseif (apple_position == 2) and (east_toggle == true) then
				goto retry_apple
			end

			if (apple_position == 3) and (south_toggle == false) then
				south_toggle = true
				minetest.set_node(south, {name = node_fruit})
			
			elseif (apple_position == 3) and (south_toggle == true) then
				goto retry_apple
			end

			if (apple_position == 4) and (west_toggle == false) then
				west_toggle = true
				minetest.set_node(west, {name = node_fruit})
			
			elseif (apple_position == 4) and (west_toggle == true) then
				goto retry_apple
			end
		end
	end

	-- Tree's crown

	-- First strate

	local row = {x = (pos.x - 3), y = (pos.y + 1), z = (pos.z + 3)}

	for n = 1, 7 do
		minetest.set_node(row, {name = node_leaves})
		row.x = row.x + 1
	end

	row = {x = (pos.x - 3), y = (pos.y + 1), z = (pos.z + 2)}

	for n = 1, 7 do
		minetest.set_node(row, {name = node_leaves})
		row.x = row.x + 1
	end

	row = {x = (pos.x - 3), y = (pos.y + 1), z = (pos.z + 1)}

	for n = 1, 7 do
		minetest.set_node(row, {name = node_leaves})
		row.x = row.x + 1
	end

	row = {x = (pos.x - 3), y = (pos.y + 1), z = pos.z}

	for n = 1, 7 do
		minetest.set_node(row, {name = node_leaves})
		row.x = row.x + 1
	end

	row = {x = (pos.x - 3), y = (pos.y + 1), z = (pos.z - 1)}

	for n = 1, 7 do
		minetest.set_node(row, {name = node_leaves})
		row.x = row.x + 1
	end

	row = {x = (pos.x - 3), y = (pos.y + 1), z = (pos.z - 2)}

	for n = 1, 7 do
		minetest.set_node(row, {name = node_leaves})
		row.x = row.x + 1
	end

	row = {x = (pos.x - 3), y = (pos.y + 1), z = (pos.z - 3)}

	for n = 1, 7 do
		minetest.set_node(row, {name = node_leaves})
		row.x = row.x + 1
	end

	-- Second strate

	row = {x = (pos.x - 2), y = (pos.y + 2), z = (pos.z + 2)}

	for n = 1, 5 do
		minetest.set_node(row, {name = node_leaves})
		row.x = row.x + 1
	end

	row = {x = (pos.x - 2), y = (pos.y + 2), z = (pos.z + 1)}

	for n = 1, 5 do
		minetest.set_node(row, {name = node_leaves})
		row.x = row.x + 1
	end

	row = {x = (pos.x - 2), y = (pos.y + 2), z = pos.z}

	for n = 1, 5 do
		minetest.set_node(row, {name = node_leaves})
		row.x = row.x + 1
	end

	row = {x = (pos.x - 2), y = (pos.y + 2), z = (pos.z - 1)}

	for n = 1, 5 do
		minetest.set_node(row, {name = node_leaves})
		row.x = row.x + 1
	end

	row = {x = (pos.x - 2), y = (pos.y + 2), z = (pos.z - 2)}

	for n = 1, 5 do
		minetest.set_node(row, {name = node_leaves})
		row.x = row.x + 1
	end

	-- Third strate

	row = {x = (pos.x - 1), y = (pos.y + 3), z = (pos.z + 1)}

	for n = 1, 3 do
		minetest.set_node(row, {name = node_leaves})
		row.x = row.x + 1
	end

	row = {x = (pos.x - 1), y = (pos.y + 3), z = pos.z}

	for n = 1, 3 do
		minetest.set_node(row, {name = node_leaves})
		row.x = row.x + 1
	end

	row = {x = (pos.x - 1), y = (pos.y + 3), z = (pos.z - 1)}

	for n = 1, 3 do
		minetest.set_node(row, {name = node_leaves})
		row.x = row.x + 1
	end
end



minetest.register_node("disk_lord:sapient_appletree", {
	description = "Sapient Apple Tree",
	tiles = {
		"default_tree_top.png^[colorize:magenta:95",
		"default_tree_top.png^[colorize:magenta:95",
		"default_tree.png^[colorize:magenta:95"
	},
	paramtype2 = "facedir",
	is_ground_content = false,
	groups = {choppy = 1, level = 2},
	light_source = 7,
	sounds = default.node_sound_wood_defaults(),
	on_blast = function() end,
	on_place = minetest.rotate_node
})

minetest.register_node("disk_lord:sapient_wood", {
	description = "Sapient Wooden Planks",
	paramtype2 = "facedir",
	place_param2 = 0,
	tiles = {
		"default_wood.png^[colorize:magenta:95"
	},
	is_ground_content = false,
	groups = {choppy = 1, level = 2},
	light_source = 7,
	sounds = default.node_sound_wood_defaults(),
	on_blast = function() end,
})

minetest.register_node("disk_lord:sapient_leaves", {
	description = "Sapient Apple Tree Leaves",
	drawtype = "allfaces_optional",
	waving = 1,
	tiles = {"default_leaves.png^[colorize:magenta:95"},
	special_tiles = {"default_leaves_simple.png^[colorize:magenta:95"},
	paramtype = "light",
	is_ground_content = false,
	groups = {snappy = 3, level = 2, leafdecay = 3, leaves = 1},
	light_source = 7,
	drop = {
		max_items = 1,
		items = {
			{
				items = {"disk_lord:sapient_stick"},
				rarity = 40,
			},
			{
				items = {"disk_lord:sapient_leaves"},
			}
		}
	},

	sounds = default.node_sound_leaves_defaults(),
	on_blast = function() end,
	after_place_node = default.after_place_leaves,
})

minetest.register_node("disk_lord:sapient_apple", {
	description = "Sapient Apple",
	drawtype = "plantlike",
	tiles = {"default_apple.png^[colorize:blue:95"},
	inventory_image = "default_apple.png^[colorize:blue:95",
	paramtype = "light",
	sunlight_propagates = true,
	walkable = false,
	is_ground_content = false,
	selection_box = {
		type = "fixed",
		fixed = {-3 / 16, -7 / 16, -3 / 16, 3 / 16, 4 / 16, 3 / 16}
	},
	groups = {fleshy = 3, dig_immediate = 3,
		leafdecay = 3, leafdecay_drop = 1},
	light_source = 7,
	on_use = minetest.item_eat(math.random(-19, 99)),
	sounds = default.node_sound_leaves_defaults(),
	on_blast = function() end,
	after_place_node = function(pos, placer, itemstack)
		if placer:is_player() then
			minetest.set_node(pos, {name = "disk_lord:sapient_apple",
				param2 = 1})
		end
	end,
})

minetest.register_node("disk_lord:sapient_sapling", {
	description = "Sapient Apple Tree Sapling",
	drawtype = "plantlike",
	tiles = {"default_sapling.png^[colorize:magenta:95"},
	inventory_image = "default_sapling.png^[colorize:magenta:95",
	wield_image = "default_sapling.png^[colorize:magenta:95",
	paramtype = "light",
	sunlight_propagates = true,
	walkable = false,
	on_timer = default.grow_sapling,
	selection_box = {
		type = "fixed",
		fixed = {-4 / 16, -0.5, -4 / 16, 4 / 16, 7 / 16, 4 / 16}
	},
	groups = {snappy = 2, dig_immediate = 3, flammable = 2,
		attached_node = 1, sapling = 1},
	sounds = default.node_sound_leaves_defaults(),
	on_blast = function() end,
	on_construct = function(pos)
		generate_sapient_tree(pos)
	end,
})

minetest.register_craftitem("disk_lord:sapient_stick", {
	description = "Sapient Stick",
	inventory_image = "default_stick.png^[colorize:magenta:95"
})

minetest.register_craft({
	output = "disk_lord:sapient_sapling",
	recipe = {
		{"disk_lord:octiron_block",
		"disk_lord:octiron_block",
		"disk_lord:octiron_block"},
		{"disk_lord:octiron_block",
		"default:sapling",
		"disk_lord:octiron_block"},
		{"disk_lord:octiron_block",
		"disk_lord:octiron_block",
		"disk_lord:octiron_block"}
	}
})

minetest.register_craft({
	output = "disk_lord:sapient_wood 2",
	recipe = {
		{"disk_lord:sapient_appletree"},
	}
})

minetest.register_craft({
	output = "disk_lord:sapient_stick",
	recipe = {
		{"disk_lord:sapient_wood"},
	}
})

minetest.register_craft({
	output = "disk_lord:luggage",
	recipe = {
		{"disk_lord:sapient_wood",
		"disk_lord:sapient_wood",
		"disk_lord:sapient_wood"},
		{"disk_lord:sapient_wood", "", "disk_lord:sapient_wood"},
		{"disk_lord:sapient_wood",
		"disk_lord:sapient_wood",
		"disk_lord:sapient_wood"},
	}
})

minetest.register_alias("SapientTree", "disk_lord:sapient_appletree")
minetest.register_alias("SapientWood", "disk_lord:sapient_wood")
minetest.register_alias("SapientLeaves", "disk_lord:sapient_leaves")
minetest.register_alias("SapientStick", "disk_lord:sapient_stick")

